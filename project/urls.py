from django.conf import settings
from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from tastypie.api import Api
from menus.resources import MenuResource
from opinions.resources import OpinionResource
from places.resources import CityResource, PlaceResource
from profiles.resources import ProfileResource

v1_api = Api(api_name='v1')
v1_api.register(CityResource())
v1_api.register(MenuResource())
v1_api.register(OpinionResource())
v1_api.register(PlaceResource())
v1_api.register(ProfileResource())

urlpatterns = patterns('',
    url(r'', include('social_auth.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
    url(r'^api/doc/', include('tastypie_swagger.urls', namespace='tastypie_swagger')),

    url(r'^dashboard/', include('dashboard.urls')),
    url(r'', include('interface.urls')),
    url(r'^profiles/', include('profiles.urls')),
    
    url(r'^accounts/', include('userena.urls')),
)

urlpatterns += patterns('',
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    )
