from django.contrib import admin
from django.contrib.contenttypes import generic

from opinions.models import Opinion

class OpinionAdmin(admin.ModelAdmin):
    pass

class OpinionInline(generic.GenericTabularInline):
    model = Opinion

admin.site.register(Opinion, OpinionAdmin)
