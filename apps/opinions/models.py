from datetime import datetime
from django.db import models

from django.contrib.auth.models import User
from django.contrib.contenttypes.generic import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class Opinion(models.Model):

    author = models.ForeignKey(User, related_name="opinions")
    submitted  = models.DateTimeField(auto_now_add=True)

    content_type = models.ForeignKey(ContentType)
    object_id = models.IntegerField()
    content_object = GenericForeignKey()

    rating = models.PositiveIntegerField(null=True, blank=True)
    comment = models.TextField(blank=True)

    def __unicode__(self):
        return u"%s rating: %s comment: %s" % (self.content_object, self.rating, self.comment)
