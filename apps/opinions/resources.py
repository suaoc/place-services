from tastypie.resources import ModelResource
from tastypie import fields, utils

from misc.tastypie_auth import DjangoAuthentication
from opinions.models import Opinion
from places.resources import PlaceResource
from profiles.resources import ProfileResource
from tastypie.authentication import ApiKeyAuthentication, MultiAuthentication
from tastypie.authorization import Authorization

class OpinionResource(ModelResource):

    author = fields.ForeignKey(ProfileResource, 'author', full=True, blank=True)
    place = fields.ForeignKey(PlaceResource, 'content_object')

    class Meta:
        always_return_data = True
        authentication = MultiAuthentication(DjangoAuthentication(), ApiKeyAuthentication())
        authorization = Authorization()
        detail_allowed_methods = ['get',]
        fields = ['author', 'comment', 'place', 'rating']
        filtering = {
            'author': ('exact',),
            }
        list_allowed_methods = ['get', 'post',]
        queryset = Opinion.objects.filter(author__is_active=True)
        resource_name = 'opinions'

    def hydrate(self, bundle):
        bundle.obj.author = bundle.request.user
        return bundle

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}

        orm_filters = super(OpinionResource, self).build_filters(filters)

        if 'parent_content_type' in filters and 'parent_object_id' in filters:
            orm_filters['content_type'] = filters['parent_content_type']
            orm_filters['object_id'] = filters['parent_object_id']

        return orm_filters
