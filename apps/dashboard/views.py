from django.contrib.admin.views.decorators import staff_member_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import get_object_or_404, redirect, render_to_response
from django.template import RequestContext

from menus.forms import MenuFormSet, MenuItemFormSet, DealFormSet
from menus.models import Menu, MenuGroup, Deal
from places.models import Place
from places.forms import PlaceForm

@staff_member_required
def DashboardView(request):
    return render_to_response("dashboard.html", {
                              }, context_instance=RequestContext(request))


@staff_member_required
def PlaceListView(request):

    search_query = request.GET.get('search_query', None)
    places_list = Place.objects.live(search_query=search_query).order_by('-id')

    paginator = Paginator(places_list, 10) # Show x places per page

    page = request.GET.get('page')
    try:
        places = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        places = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        places = paginator.page(paginator.num_pages)

    return render_to_response('place_list.html', {
                              'places': places,
                              'search_query': search_query,
                              }, context_instance=RequestContext(request))

@staff_member_required
def PlaceEditView(request, place_id=None):

    if place_id is None:
        place = None
    else:
        place = get_object_or_404(Place, id=place_id)

    if request.method == 'POST':
        form = PlaceForm(request.POST, instance=place)
        
        if form.is_valid():
            place = form.save()
            form = PlaceForm(instance=place)

    else:
        form = PlaceForm(instance=place)

    return render_to_response("place_edit.html", {
                              'place': form,
                              }, context_instance=RequestContext(request))

@staff_member_required
def PlaceMenusEditView(request, place_id):

    place = get_object_or_404(Place, id=place_id)

    if request.method == 'POST':
        formset = MenuFormSet(request.POST, instance=place)

        if formset.is_valid():
            menus = formset.save_all()
            formset = MenuFormSet(instance=place)

    else:
        formset = MenuFormSet(instance=place)

    return render_to_response('place_menus_edit.html', {
                              'place': place,
                              'menus': formset,
                              }, context_instance=RequestContext(request))

@staff_member_required
def GroupItemsEditView(request, group_id):

    group = get_object_or_404(MenuGroup, id=group_id)

    if request.method == 'POST':
        formset = MenuItemFormSet(request.POST, instance=group)

        if formset.is_valid():
            menus = formset.save_all()
            formset = MenuItemFormSet(instance=group)

    else:
        formset = MenuItemFormSet(instance=group)

    return render_to_response('group_items_edit.html', {
                              'group': group,
                              'items':formset,
                              }, context_instance=RequestContext(request))

@staff_member_required
def PlaceDealsEditView(request, place_id):

    place = get_object_or_404(Place, id=place_id)

    if request.method == 'POST':
        formset = DealFormSet(request.POST, instance=place)

        if formset.is_valid():
            deals = formset.save_all()
            formset = DealFormSet(instance=place)

    else:
        formset = DealFormSet(instance=place)

    return render_to_response('place_deals_edit.html', {
            'place': place,
            'deals': formset,
            }, context_instance=RequestContext(request))

