

var ScrollFix = function(elem) {
    // Variables to track inputs
    var startY, startTopScroll;

    elem = elem || document.querySelector(elem);

    // If there is no element, then do nothing  
    if(!elem)
        return;

    // Handle the start of interactions
    elem.addEventListener('touchstart', function(event){
        startY = event.touches[0].pageY;
        startTopScroll = elem.scrollTop;

        if(startTopScroll <= 0)
            elem.scrollTop = 1;

        if(startTopScroll + elem.offsetHeight >= elem.scrollHeight)
            elem.scrollTop = elem.scrollHeight - elem.offsetHeight - 1;
    }, false);
};

var currentColumn = 1;
var defaultTimeout = 55000;
var columnTemplate = '<div class="content-view"></div><div class="bar-top"><button class="btn back-button">Back</button></div>';

var pushColumn = function() {
    if ($(window).width() > 960) return;
    if (currentColumn == 1) {
        $('.content-two').html('');
        $('.column-two').css({'display' : 'block', 'left' : '150%'})
        $('.column-two').animate({'left': '0px'}, 'medium', 'swing', function() { $('.column-one').hide(); });
        $('.column-three').hide();
        currentColumn = 2;
    } else if (currentColumn == 2) {
        $('.column-one').hide();
        $('.content-three').html('');
        $('.column-three').css({'display' : 'block', 'left' : '150%'})
        $('.column-three').animate({'left': '0px'}, 'medium', 'swing', function() { $('.column-two').hide(); });
        currentColumn = 3;
    }
}

var popColumn = function() {
    if ($(window).width() > 960) return;
    if (currentColumn == 2) {
        $('.column-one').show();
        $('.column-two').animate({'left': '100%'}, 'slow', 'easeInCubic', function() { $('.column-two').hide(); });
        $('.column-three').hide();
        currentColumn = 1;
    } else if (currentColumn == 3) {
        $('.column-one').hide();
        $('.column-two').show();
        $('.column-three').animate({'left': '100%'}, 'slow', 'easeInCubic', function() { $('.column-three').hide(); });
        currentColumn = 2;
    }
}

var updateColumnsState = function() {
    if ($(window).width() > 960) {
        currentColumn = 1;
        $('.column-one').css('left', '0%');
        $('.column-one').show();
        $('.column-two').css('left', '33%');
        $('.column-two').show();
        $('.column-three').css('left', '66%');
        $('.column-three').show();
    } else {
        if (currentColumn == 1) {
            $('.column-one').show();
            $('.column-two').hide();
            $('.column-three').hide();
        } else if (currentColumn == 2) {
            $('.column-one').hide();
            $('.column-two').show();
            $('.column-three').hide();
        } else if (currentColumn == 3) {
            $('.column-one').hide();
            $('.column-two').hide();
            $('.column-three').show();
        }
    }
}

var showPlace = function() {
    $(this).trigger('pushColumn');
    $('.content-two').html(columnTemplate);

    $.pjax({
        url: $(this).attr('href'),
        container: '.content-two',
        push: false,
        timeout: defaultTimeout,
        success: function() {
            setTimeout(setupMap, 200);
        },
    })

    $('.content-one .selected').removeClass("selected");
    $(this).closest(".nested-item").addClass("selected");
    $('.content-three').html(columnTemplate);

    return false;
}

var showPlaceMenus = function() {
    $(this).trigger('pushColumn');
    $('.content-two').html(columnTemplate);

    $.pjax({
        url: $(this).attr('href'),
        container: '.content-two',
        push: false,
        timeout: defaultTimeout,
        success: function() {
            setupMenuForms();
        },
    })

    $('.content-one .selected').removeClass("selected");
    $(this).closest(".nested-item").addClass("selected");
    $('.content-three').html(columnTemplate);

    return false;
}

var showGroupItems = function() {
    $(this).trigger('pushColumn');
    $('.content-three').html(columnTemplate);

    $.pjax({
        url: $(this).attr('href'),
        container: '.content-three',
        push: false,
        timeout: defaultTimeout,
        success: function() {
            setupItemForms();
        },
    })

    $('.content-two .highlighted').removeClass("highlighted");
    $(this).closest(".nested-item").addClass("highlighted");

    return false;
}

var showPlaceDeals = function() {
    $(this).trigger('pushColumn');
    $('.content-two').html(columnTemplate);

    $.pjax({
        url: $(this).attr('href'),
        container: '.content-two',
        push: false,
        timeout: defaultTimeout,
        success: function() {
            setupDealForms();
        },
    })

    $('.content-one .selected').removeClass("selected");
    $(this).closest(".nested-item").addClass("selected");
    $('.content-three').html(columnTemplate);

    return false;
}

var savePlace = function() {
    form = $('.place-form')
    $.pjax({
        url: form.attr('action'),
        container: '.content-two',
        data: form.serialize(),
        type:'POST',
        push:false,
        timeout: defaultTimeout,
        success: function() {
            setTimeout(setupMap, 200);
        },

    })
    return false;
}

var savePlaceMenus = function() {
    form = $('.place-menus-form')
    $.pjax({
        url: form.attr('action'),
        container: '.content-two',
        data: form.serialize(),
        type:'POST',
        push:false,
        timeout: defaultTimeout,
        success: function() {
            setupMenuForms();
        },
    })

    $('.content-three').html(columnTemplate);

    return false;
}

var saveGroupItems = function() {
    form = $('.group-items-form')
    $.pjax({
        url: form.attr('action'),
        container: '.content-three',
        data: form.serialize(),
        type:'POST',
        push:false,
        timeout: defaultTimeout,
        success: function() {
            setupItemForms();
        },
    })
    return false;
}

var savePlaceDeals = function() {
    form = $('.place-deals-form')
    $.pjax({
        url: form.attr('action'),
        container: '.content-two',
        data: form.serialize(),
        type:'POST',
        push:false,
        timeout: defaultTimeout,
        success: function() {
            setupDealForms();
        },
    })

    $('.content-three').html(columnTemplate);

    return false;
}

var refreshPlaceList = function() {
    $('.content-one .content-view').html('');
    query_string = $('#search-input').val();
    $.pjax({
        url: placelisturl,
        container: '.content-one',
        push: false,
        timeout: defaultTimeout,
        data: {'search_query' : query_string},
    });
}

var doPlaceSearch = function(e) {
    if (e.keyCode == 13) refreshPlaceList();
}

var showMorePlaces = function() {
    var query_string = $('#search-input').val();
    var page = $(this).attr('page');
    var page_id = 'page-' + page;
    var content_view = $(this).closest('.content-view')
    content_view.append('<ul id=' + page_id  + ' class="root-items unstyled"></ul>');
    load_more_button = $(this).parent();
    $.pjax({
        url: placelisturl,
        container: '#' + page_id,
        push: false,
        timeout: defaultTimeout,
        data: {'search_query' : query_string, 'page' : page },
        success: function() {
            load_more_button.remove();
            scroll($('#'+ page_id), content_view);
        },
    });
}

var requestStart = function(e, xhr, options) {
    var container = $(options.container);
    var parent = container.closest('.column');
    //parent.find('.overlay').fadeIn('fast');
    parent.spin();
    container.find('.content-view').fadeOut();
}

var requestEnd = function(e, xhr, options) {
    var container = $(options.container);
    var parent = $(options.container).closest('.column');
    container.scrollTop(0);
    container.find('.content-view').fadeIn('fast');
    parent.spin(false);
    //parent.find('.overlay').fadeOut('fast');
    container.find('textarea').each(sizeTextArea);
    if (container.find('.has-errors').length > 0) container.find('.errors-message').show();
}

var requestError = function(e, xhr, textStatus, errorThrown, options) {
    if (options.type == 'GET') {
        var message = 'Unable to fetch data. Please try again.'
    } else if (options.type == 'POST') {
        var message = 'Unable to save data. Please try again.'
    }
    var container = $(options.container);
    if (container.find('.message').length > 0) container.find('.message').html(message);
    else container.find('.content-view').prepend('<div class=\'message\'>' + message + '</div>');

    return false;
}

$(function() {

    $('.content-one').html(columnTemplate);
    $('.content-two').html(columnTemplate);
    $('.content-three').html(columnTemplate);

    $('.column-view').each(function(i, e) { new ScrollFix(e); });

    $(document).on('pushColumn', pushColumn);
    $(document).on('click', '.back-button', popColumn);
    $(window).resize(updateColumnsState);
    $(document).on('orientationchange', updateColumnsState);

    $(document)
        .on('pjax:start', requestStart)
        .on('pjax:end', requestEnd)
        .on('pjax:error', requestError);

    $('.content-one').on('click', '#refresh-places-button', refreshPlaceList);
    $('.content-one').on('keypress', '#search-input', doPlaceSearch);
    $('.content-one').on('click', '.add-place-button', showPlace);
    $('.content-one').on('click', '.show-more-places-button', showMorePlaces);

    $('.content-one').on('click', '.show-place', showPlace);
    $('.content-one').on('click', '.show-place-menus', showPlaceMenus);
    $('.content-two').on('click', '.show-group-items', showGroupItems);
    $('.content-one').on('click', '.show-place-deals', showPlaceDeals);

    $('.content-two').on('click', '.save-place-button', savePlace);
    $('.content-two').on('click', '.save-place-menus-button', savePlaceMenus);
    $('.content-three').on('click', '.save-group-items-button', saveGroupItems);
    $('.content-two').on('click', '.save-place-deals-button', savePlaceDeals);

    $(document)
        .on('click', '.add-menu-button', addMenu)
        .on('click', '.add-menu-group-button', addMenuGroup)
        .on('click', '.add-menu-item-button', addMenuItem)
        .on('click', '.add-menu-item-size-button', addMenuItemSize)
        .on('click', '.add-deal-button', addDeal)
        .on('click', '.add-deal-item-button', addDealItem)
        .on('focus', '.size-name-input', setupItemSizeNameField)
        .on('focus', 'textarea', sizeTextArea)
        .on('keyup', 'textarea', sizeTextArea)

    refreshPlaceList();
});

$.fn.spin = function(opts) {
    this.each(function() {
        var $this = $(this),
        data = $this.data();

        if (data.spinner) {
            data.spinner.stop();
            delete data.spinner;
        }
        if (opts !== false) {
            data.spinner = new Spinner($.extend({color: '#4DA7EB', className: 'spinner',}, opts)).spin(this);
        }
    });
    return this;
};

var setupMap = function() {

    var context = $('.content-two .root-formset');

    var latInput = context.find('.place-latitude-input').first();
    var lngInput = context.find('.place-longitude-input').first();

    var default_coordinates = new google.maps.LatLng(31.54505, 74.340683);
    var has_original_coordinates = true;
    var original_latitude = parseFloat(latInput.val());
    var original_longitude = parseFloat(lngInput.val());

    var initial_zoom = 15;
    var deep_zoom = 18;

    if (isNaN(original_latitude) || isNaN(original_longitude)) {
        latInput.val('');
        lngInput.val('');
        context.find('.remove-location-button').attr('disabled', 'disabled');
        has_original_coordinates = false;
        original_coordinates = default_coordinates;
    } else {
        original_coordinates = new google.maps.LatLng(original_latitude, original_longitude);
        initial_zoom = deep_zoom;
    }

    context.find('.reset-location-button').attr('disabled', 'disabled');

    var mapOptions = {
        center: original_coordinates,
        zoom: initial_zoom,
        mapTypeId: google.maps.MapTypeId.HYBRID
    };
    var map = new google.maps.Map(context.find('.place-map-canvas').get(0), mapOptions);

    var autocompleteOptions = {
        //componentRestrictions: {country: 'pk'}
    };
    var autocomplete = new google.maps.places.Autocomplete(context.find('.place-search-input').get(0), autocompleteOptions);
    context.find('.place-search-input').focus(function() {
        $(this).val('');
    });

    var marker = new google.maps.Marker({
        position: original_coordinates,
        map: map,
        draggable: true,
    });

    google.maps.event.addListener(marker,'dragend',function(event) {
        setFormCoordinates(event.latLng);
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        marker.setPosition(place.geometry.location);
        setFormCoordinates(place.geometry.location);
        map.setZoom(deep_zoom);
    });

    context.on('click', '.remove-location-button', function() {
        latInput.val('');
        lngInput.val('');
        $(this).attr('disabled', 'disabled');
        if (has_original_coordinates) {
            context.find('.reset-location-button').removeAttr('disabled');
        }
        return false;
    });

    context.on('click', '.reset-location-button', function() {
        if (has_original_coordinates) {
            latlng = new google.maps.LatLng(original_latitude, original_longitude);
            marker.setPosition(latlng);
            setFormCoordinates(latlng);
        }
        $(this).attr('disabled', 'disabled');
        return false;
    });

    var watchProcess = null;
    context.on('click', '.set-to-current-location-button', function() {
        if (navigator.geolocation) {
            watchProcess = navigator.geolocation.watchPosition(function(position) {
                latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                marker.setPosition(latlng);
                map.setZoom(deep_zoom);
                setFormCoordinates(latlng);
            }, function(error) {
                switch(error.code) {
                case error.PERMISSION_DENIED: console.log("user did not share geolocation data");
                    break;
                case error.POSITION_UNAVAILABLE: console.log("could not detect current position");
                    break;
                case error.TIMEOUT: console.log("retrieving position timed out");
                    break;
                default: console.log("unknown error");
                    break;
                }
            }, {enableHighAccuracy:true, maximumAge:30000, timeout:120000});
        }
        return false;
    });

    var setFormCoordinates = function(latLng) {
        if (watchProcess != null) {
            navigator.geolocation.clearWatch(watchProcess);
            watchProcess = null;
        }
        context.find('#id_latitude').val(latLng.lat());
        context.find('#id_longitude').val(latLng.lng());

        if (map && marker && !map.getBounds().contains(marker.getPosition())) {
            map.setCenter(latLng);
        }

        context.find('.remove-location-button').removeAttr('disabled');
        if (has_original_coordinates) {
            context.find('.reset-location-button').removeAttr('disabled');
        }
    }
}

var AverageMenus = 1;
var AverageMenuGroups = 5;
var AverageMenuItems = 6;
var AverageMenuItemSizes = 1
var AverageDeals = 3;
var AverageDealItems = 4;

var setupMenuForms = function() {
    var parent = $('.content-two');

    var all_forms = parent.find('.root-items > .root-item-wrapper');
    all_forms.each(function() {
        var initial_forms = $(this).find('.nested-items > .initial');
        for (i = initial_forms.length; i < AverageMenuGroups; i++) {
            addNewNestedForm($(this).find('.nested-formset'), false);
        }
    });

    var initial_forms = parent.find('.root-items > .initial');
    for (i = initial_forms.length; i < AverageMenus; i++) {
        addNewRootForm(parent, false);
    }
}

var setupItemForms = function() {
    var parent = $('.content-three');

    var all_forms = parent.find('.root-items > .root-item-wrapper');
    all_forms.each(function() {
        var initial_forms = $(this).find('.nested-items > .initial');
        for (i = initial_forms.length; i < AverageMenuItemSizes; i++) {
            addNewNestedForm($(this).find('.nested-formset'), false);
        }
    });

    var initial_forms = parent.find('.root-items > .initial');
    for (i = initial_forms.length; i < AverageMenuItems; i++) {
        addNewRootForm(parent, false);
    }
}

var setupDealForms = function() {
    var parent = $('.content-two');

    var all_forms = parent.find('.root-items > .root-item-wrapper');
    all_forms.each(function() {
        var initial_forms = $(this).find('.nested-items > .initial');
         for (i = initial_forms.length; i < AverageDealItems; i++) {
            addNewNestedForm($(this).find('.nested-formset'), false);
        }
    });

    var initial_forms = parent.find('.root-items > .initial');
    for (i = initial_forms.length; i < AverageDeals; i++) {
        addNewRootForm(parent, false);
    }
}

var addNewRootForm = function(parent, animated) {
    duration = 0;
    if (animated) duration = 400; 

    var empty_forms = parent.find('.root-items > .extra');

    var first_empty = empty_forms.first();
    first_empty.show('blind', duration, function() {
        first_empty.removeClass('extra');
        first_empty.addClass('new');
    });

    if (empty_forms.length <= 1) {
        parent.find('.add-root-item-button').hide('fast');
    }

    return first_empty;
}

var addNewNestedForm = function(parent, animated) {
    duration = 0;
    if (animated) duration = 400; 

    var empty_forms = parent.find('.nested-items > .extra');

    var first_empty = empty_forms.first();
    first_empty.show('blind', duration, function() {
        first_empty.removeClass('extra');
        first_empty.addClass('new');
    });

    if (empty_forms.length <= 1) {
        parent.find('.add-nested-item-button').hide('fast');
    }

    return first_empty;
}

function scroll(element, parent){
    var element_top = $(element).offset().top;
    var element_height = $(element).height();
    var element_bottom = element_top + element_height;
    var parent_top = $(parent).offset().top;
    var parent_height = $(parent).height();
    var parent_bottom = parent_top + parent_height;
    var scroll_distance = $(parent).scrollTop();

    if (element_height < parent_height) {
        if (element_top < parent_top) scroll_distance = scroll_distance - (parent_top - element_top);
        else if (element_top > parent_top && element_bottom < parent_bottom) return;
        else if (element_bottom > parent_bottom) {
            var padding = 90;
            var height_difference = parent_height - element_height;
            if (height_difference < padding) padding = height_difference;
            scroll_distance = scroll_distance + (element_top - parent_top) - (parent_height - element_height) + padding;
        }
    } else {
        if (element_top < parent_top) scroll_distance = scroll_distance - (parent_top - element_top);
        else if (element_top > parent_top) scroll_distance = scroll_distance + (element_top - parent_top);
    }

    $(parent).animate({ scrollTop: scroll_distance }, { duration: 400, easing: 'swing'});
}

var addMenu = function() {
    var el = addNewRootForm($(this).closest('.root-formset'), true);
    if (el.length > 0) scroll(el, el.closest('.content-view'));
    return false;
}

var addMenuGroup = function() {
    var el = addNewNestedForm($(this).closest('.nested-formset'), true);
    if (el.length > 0) scroll(el, el.closest('.content-view'));
    return false;
}

var addMenuItem = function() {
    var el = addNewRootForm($(this).closest('.root-formset'), true);
    if (el.length > 0) scroll(el, el.closest('.content-view'));
    return false;
}

var addMenuItemSize = function() {
    var el = addNewNestedForm($(this).closest('.nested-formset'), true);
    if (el.length > 0) scroll(el, el.closest('.content-view'));
    return false;
}

var addDeal = function() {
    var el = addNewRootForm($(this).closest('.root-formset'), true);
    if (el.length > 0) scroll(el, el.closest('.content-view'));
    return false;
}

var addDealItem = function() {
    var el = addNewNestedForm($(this).closest('.nested-formset'), true);
    if (el.length > 0) scroll(el, el.closest('.content-view'));
    return false;
}

var setupItemSizeNameField = function() {
    if ($(this).val() == '') $(this).val('Standard');
}

var sizeTextArea = function() {
    if ($(this).val() != '') {
        $(this).height($(this).prop('scrollHeight'));
    }
}
