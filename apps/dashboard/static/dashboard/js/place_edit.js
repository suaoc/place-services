var App = Em.Application.create();

App.store = DS.Store.create({
    revision: 4,
    adapter: DS.DjangoTastypieAdapter.create({
        serverDomain: "http://localhost:8000/",
        tastypieApiUrl: "api/v1/",
    })
});

App.Places = DS.Model.extend({
    restaurant_name: DS.attr('string'),
    brief_description: DS.attr('string'),
    menus: DS.hasMany('App.Menu'),

    didLoad: function() {
        console.log(this.get('restaurant_name'));
    }
});

App.Menus = DS.Model.extend({
    name: DS.attr('string'),
    description: DS.attr('string'),
    place: DS.belongsTo('App.Place'),
});


App.menusController = Ember.ArrayController.create({
  // The array of Contact objects that backs the array controller.
  content: [],

  // Adds a new contact to the list and ensures it is
  // sorted correctly.
  add: function(menu) {
    var length = this.get('length'), idx;

    //idx = this.binarySearch(contact.get('sortValue'), 0, length);

    this.insertAt(idx, menu);

    // If the value by which we've sorted the contact
    // changes, we need to re-insert it at the correct
    // location in the list.
    //contact.addObserver('sortValue', this, 'contactSortValueDidChange');
  },

  remove: function(menu) {
    this.removeObject(menu);
      //contact.removeObserver('sortValue', this, 'contactSortValueDidChange');
  },

  contactSortValueDidChange: function(menu) {
    this.remove(menu);
    this.add(menu);
  },

  // Creates a new, empty Contact object and adds it to the
  // array controller.
  newContact: function() {
    // this.add(App.Contact.create({
    //   firstName: names[firstName],
    //   lastName: hasLastName < 0.9 ? names[lastName] : null,
    //   phoneNumbers: []
    // }));
  },

  loadMenus: function() {
    this.set('content', App.store.findAll(App.Menus));
  },

});

App.menusController.loadMenus();

App.TextField = Ember.TextField.extend({
  didInsertElement: function() {
    this.$().focus();
  }
});

App.EditField = Ember.View.extend({
  tagName: 'span',
  templateName: 'edit-field',


  touchEnd: function() {
    // Rudimentary double tap support, could be improved
    var touchTime = new Date();
    if (this._lastTouchTime && touchTime - this._lastTouchTime < 250) {
      this.doubleClick();
      this._lastTouchTime = null;
    } else {
      this._lastTouchTime = touchTime;
    }

    // Prevent zooming
    return false;
  },

  focusOut: function() {
    this.set('isEditing', false);
  },

  keyUp: function(evt) {
    if (evt.keyCode === 13) {
      this.set('isEditing', false);
    }
  }
});

Ember.Handlebars.registerHelper('editable', function(path, options) {
  options.hash.contentBinding = path;
  return Ember.Handlebars.helpers.view.call(this, App.EditField, options);
});

App.MenuView = Em.View.extend({
  templateName: 'menu-view',
  preserveContext: true,
  hello: 'here',

  doubleClick: function() {
    this.set('isEditing', true);
    console.log(this.content);
    console.log(this.get('name'));
    App.men = this;
    return false;
  },

});

App.MenusView = Em.View.extend({
  templateName: 'menus-view',
  contentBinding: 'App.menusController.content',


});
