from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('',
                       url(r'^$', 'dashboard.views.DashboardView', name='dashboard_view'),
                       url(r'^places/$', 'dashboard.views.PlaceListView', name='place_list'),
                       url(r'^places/add/$', 'dashboard.views.PlaceEditView', name='place_add'),
                       url(r'^places/(?P<place_id>[\d]+)/$', 'dashboard.views.PlaceEditView', name='place_edit'),
                       url(r'^place-menus/(?P<place_id>[\d]+)/$', 'dashboard.views.PlaceMenusEditView', name='place_menus_edit'),
                       url(r'^group-items/(?P<group_id>[\d]+)/$', 'dashboard.views.GroupItemsEditView', name='group_items_edit'),
                       url(r'^place-deals/(?P<place_id>[\d]+)/$', 'dashboard.views.PlaceDealsEditView', name='place_deals_edit'),
                       )

