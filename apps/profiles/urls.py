from django.conf.urls.defaults import patterns, url

from profiles.api import auth_clientflow

urlpatterns = patterns('',
                       # authentication
                       url(r'^auth/(?P<backend>[^/]+)/$', auth_clientflow,
                           name='auth_clientflow'),
                       url(r'^delegate/$', 'profiles.views.delegate',
                           name='delegate'),
                       )
