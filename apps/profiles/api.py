from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login
from django import http
from django.utils import simplejson as json
from social_auth.decorators import dsa_view
from social_auth.views import auth_complete

@csrf_exempt
@dsa_view()
def auth_clientflow(request, backend, *args, **kwargs):
    """Authentication complete process"""
    response = http.HttpResponseServerError()
    user = auth_complete(request, backend, *args, **kwargs)

    if isinstance(user, http.HttpResponse):
        response = user

    if user:
        if getattr(user, 'is_active', True):
            login(request, user)

            api_key = user.api_key.key

            response = http.HttpResponse(json.dumps({
                        "name": user.get_full_name(),
                        "username": user.username,
                        "api_key": api_key,
                        }), mimetype="application/json")
        else:
            response = http.HttpResponseForbidden()

    return response
