from userena.utils import get_profile_model
from userena.managers import ASSIGNED_PERMISSIONS
from guardian.shortcuts import assign, get_perms


def create_userena_profile(backend, details, response, user, is_new=False, *args,  **kwargs):

    if not is_new:
        return None

    new_user = user

    # Taken from UserenaMnaager create_user()
    profile_model = get_profile_model()
    try:
        new_profile = new_user.get_profile()
    except profile_model.DoesNotExist:
        new_profile = profile_model(user=new_user)
        new_profile.save()

    # Give permissions to view and change profile
    for perm in ASSIGNED_PERMISSIONS['profile']:
        assign(perm[0], new_user, new_profile)

    # Give permissions to view and change itself
    for perm in ASSIGNED_PERMISSIONS['user']:
        assign(perm[0], new_user, new_user)
