from django.http import HttpResponse

def delegate(request):
    html = "<html><body></body></html>"
    return HttpResponse(html)
