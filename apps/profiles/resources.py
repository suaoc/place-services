from django.contrib.auth.models import User
from tastypie.resources import ModelResource
from tastypie import fields, utils
from tastypie.authentication import Authentication
from tastypie.authorization import ReadOnlyAuthorization

from profiles.models import Profile

class ProfileResource(ModelResource):

    name = fields.CharField(attribute='get_full_name', default='Anonymous')

    class Meta:
        always_return_data = True
        authentication = Authentication()
        authorization = ReadOnlyAuthorization()
        detail_allowed_methods = ['get',]
        fields = ['name',]
        list_allowed_methods = ['get',]
        queryset = User.objects.filter(is_active=True)
        resource_name = 'profiles'

    def dehydrate(self, bundle):
        bundle.data['username'] = bundle.obj.username
        return bundle