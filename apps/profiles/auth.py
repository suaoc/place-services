from social_auth.backends.exceptions import AuthException, AuthCanceled, AuthFailed, AuthTokenError, AuthUnknownError
from django.contrib.auth import authenticate
import cgi
from urllib import urlencode
from urllib2 import urlopen, HTTPError
from social_auth.utils import sanitize_log_data, setting, log
from social_auth.backends.facebook import FacebookBackend, FacebookAuth, ACCESS_TOKEN

class FacebookClientFlowBackend(FacebookBackend):
    name = 'facebook'

class FacebookClientFlowAuth(FacebookAuth):
    AUTH_BACKEND = FacebookClientFlowBackend

    def auth_complete(self, *args, **kwargs):
        """Completes loging process, must return user instance"""
        access_token = None
        expires = None

        if 'access_token' not in self.data:
            """Completes loging process, must return user instance"""
            if 'code' not in self.data:
                if self.data.get('error') == 'access_denied':
                    raise AuthCanceled(self)
                else:
                    raise AuthException(self)

            else:
                url = ACCESS_TOKEN + urlencode({
                        'client_id': setting('FACEBOOK_APP_ID'),
                        'redirect_uri': self.redirect_uri,
                        'client_secret': setting('FACEBOOK_API_SECRET'),
                        'code': self.data['code']
                        })
                try:
                    response = cgi.parse_qs(urlopen(url).read())
                except HTTPError:
                    raise AuthFailed(self, 'There was an error authenticating the app')

                access_token = response['access_token'][0]
                if 'expires' in response:
                    expires = response['expires'][0]

        else:
            access_token = self.data['access_token']
            if 'expires' in self.data:
                expires = self.data['expires']

        data = self.user_data(access_token)

        if self.data.get('error') == 'access_denied':
            raise AuthCanceled(self)

        if not isinstance(data, dict):
            # From time to time Facebook responds back a JSON with just
            # False as value, the reason is still unknown, but since the
            # data is needed (it contains the user ID used to identify the
            # account on further logins), this app cannot allow it to
            # continue with the auth process.
            raise AuthUnknownError(self, 'An error ocurred while ' \
                                       'retrieving users Facebook ' \
                                       'data')

        data['access_token'] = access_token
        # expires will not be part of response if offline access
        # premission was requested
        if 'expires' is not None:
            data['expires'] = expires

        kwargs.update({'auth': self,
                       'response': data,
                       self.AUTH_BACKEND.name: True})

        return authenticate(*args, **kwargs)

BACKENDS = {
    'facebook': FacebookClientFlowAuth,
    }
