from django.db import models
from django.contrib.auth.models import User
from tastypie.models import create_api_key
from userena.models import UserenaBaseProfile

# Create tastypie api-key on user creation
models.signals.post_save.connect(create_api_key, sender=User, dispatch_uid='kjnkbd')


class Profile(UserenaBaseProfile):
    user = models.OneToOneField(User,
                                unique=True,
                                related_name='profile')
