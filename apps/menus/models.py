from django.db import models
from model_utils import Choices
from places.models import Place

STATUS = Choices(('visible', 'Visible'), ('hidden', 'Hidden'), ('deleted', 'Deleted'))

class MenuManager(models.Manager):

    def get_query_set(self):
        return super(MenuManager, self).get_query_set().filter(status__in=[STATUS.visible, STATUS.hidden])


class Menu(models.Model):

    status = models.CharField(choices=STATUS, default=STATUS.visible, max_length=20)

    place = models.ForeignKey(Place, related_name='menus')
    name = models.CharField(max_length=50, help_text='format: 1-50 characters.')
    description = models.CharField(max_length=255, blank=True, help_text='format: 0-255 characters.')
    #duration_name = models.CharField(max_length=25, help_text='format: 1-25 characters.')
    duration_time_start = models.TimeField(blank=True, null=True)
    duration_time_end = models.TimeField(blank=True, null=True)

    objects = MenuManager()

    def __unicode__(self):
        return self.name


class MenuGroup(models.Model):

    status = models.CharField(choices=STATUS, default=STATUS.visible, max_length=20)

    menu = models.ForeignKey(Menu, related_name='groups')
    name = models.CharField(max_length=50, help_text='format: 1-50 characters.')
    description = models.CharField(max_length=255, blank=True, help_text='format: 0-255 characters.')

    objects = MenuManager()

    def __unicode__(self):
        return self.name


class MenuItem(models.Model):

    status = models.CharField(choices=STATUS, default=STATUS.visible, max_length=20)

    group = models.ForeignKey(MenuGroup, related_name='items')
    name = models.CharField(max_length=75, help_text='format: 1-75 characters.')
    description = models.CharField(max_length=450, blank=True, help_text='format: 0-450 characters.')

    objects = MenuManager()

    def __unicode__(self):
        return self.name


class MenuItemSize(models.Model):

    status = models.CharField(choices=STATUS, default=STATUS.visible, max_length=20)

    item = models.ForeignKey(MenuItem, related_name='sizes')
    name = models.CharField(max_length=50, help_text='format: 1-50 characters.')
    price = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True, help_text='format: 0-9999.99. Period separator for decimals.' )

    objects = MenuManager()

    def __unicode__(self):
        return self.name


# class MenuOption(models.Model):

#     name = models.CharField(max_length=50, help_text='format: 1-50 characters.')
#     description - models.CharField(max_length=255, blank=True, help_text='format: 0-255 characters.')
#     min_selected = models.IntegerField(blank=True, null=True, help_text='')
#     max_selected = models.IntegerField(blank=True, null=True, help_text='')

#     def __unicode__(self):
#         return self.name

class Deal(models.Model):

    status = models.CharField(choices=STATUS, default=STATUS.visible, max_length=20)

    place = models.ForeignKey(Place, related_name='deals')
    name = models.CharField(max_length=50, help_text='format: 1-50 characters.')
    description = models.CharField(max_length=450, blank=True, help_text='format: 0-450 characters.')
    conditions = models.CharField(max_length=450, blank=True, help_text='format: 0-450 characters.')
    price = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True, help_text='format: 0-9999.99. Period separator for decimals.' )

    objects = MenuManager()

    def __unicode__(self):
        return self.name

class DealItem(models.Model):

    status = models.CharField(choices=STATUS, default=STATUS.visible, max_length=20)

    deal = models.ForeignKey(Deal, related_name='items')
    name = models.CharField(max_length=75, help_text='format: 1-75 characters.')

    objects = MenuManager()

    def __unicode__(self):
        return self.name
