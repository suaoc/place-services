from django.forms import ModelForm, widgets
from django.forms.formsets import DELETION_FIELD_NAME
from django.forms.models import BaseInlineFormSet, inlineformset_factory, modelformset_factory

from menus.models import Menu, MenuGroup, MenuItem, MenuItemSize, Deal, DealItem
from places.models import Place


class MenuForm(ModelForm):

    class Meta:
        model = Menu
        fields = ('status', 'name', 'description',)
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'menu-name-input', 'placeholder': 'Menu Name',}),
            'description': widgets.Textarea(attrs={'class': 'menu-description-textarea', 'placeholder':'Description',}),
            }

class MenuGroupForm(ModelForm):

    class Meta:
        model = MenuGroup
        fields = ('status', 'name', 'description',)
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'group-name-input', 'placeholder': 'Group Name',}),
            'description': widgets.Textarea(attrs={'class': 'group-description-textarea', 'placeholder':'Description',}),
            }

class MenuItemForm(ModelForm):

    class Meta:
        model = MenuItem
        fields = ('status', 'name', 'description',)
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'item-name-input', 'placeholder': 'Item Name',}),
            'description': widgets.Textarea(attrs={'class': 'item-description-textarea', 'placeholder':'Description',}),
            }

class MenuItemSizeForm(ModelForm):

    class Meta:
        model = MenuItemSize
        fields = ('status', 'name', 'price',)
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'size-name-input', 'placeholder': '',}),
            'price': widgets.TextInput(attrs={'class': 'size-price-input', 'placeholder': '',}),
            }

class DealForm(ModelForm):

    class Meta:
        model = Deal
        fields = ('status', 'name', 'description', 'conditions', 'price',)
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'deal-name-input', 'placeholder': 'Deal Name',}),
            'description': widgets.Textarea(attrs={'class': 'deal-description-textarea', 'placeholder':'Description',}),
            'conditions': widgets.Textarea(attrs={'class': 'deal-conditions-textarea', 'placeholder':'Terms & Conditions',}),
            'price': widgets.TextInput(attrs={'class': 'deal-price-input', 'placeholder': '',}),
            }

class DealItemForm(ModelForm):

    class Meta:
        model = DealItem
        fields = ('status', 'name',)
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'deal-item-name-input', 'placeholder': 'Item Name',}),
            }


class NestedInlineFormSet(BaseInlineFormSet):

    nested_model_class = None
    prefix_class = 'NESTED'

    def nested_formset(self, instance, pk_value):
        return [self.nested_model_class(
                instance = instance,
                prefix = '%s_%s' % (self.prefix_class, pk_value) )]

    def add_fields(self, form, index):
        # allow the super class to create the fields as usual
        super(NestedInlineFormSet, self).add_fields(form, index)

        # created the nested formset
        try:
            instance = self.get_queryset()[index]
            pk_value = instance.pk
        except Exception, e:
            instance=None
            pk_value = hash(form.prefix)

        # store the formset in the .nested property
        if self.nested_model_class is not None: 
            form.nested = self.nested_formset(instance, pk_value)

    def is_valid(self):
        result = super(NestedInlineFormSet, self).is_valid()

        for form in self.forms:
            if hasattr(form, 'nested'):
                for n in form.nested:
                    n.data = form.data
                    if form.is_bound:
                        n.is_bound = True
                    for nform in n:
                        nform.data = form.data
                        if form.is_bound:
                            nform.is_bound = True
                    # make sure each nested formset is valid as well
                    result = result and n.is_valid()
        return result

    def save_new(self, form, commit=True):
        """Saves and returns a new model instance for the given form."""
        
        instance = super(NestedInlineFormSet, self).save_new(form, commit=commit)
        
        # update the form's instance reference
        form.instance = instance
        
        # update the instance reference on nested forms
        for nested in form.nested:
            nested.instance = instance
            
            # iterate over the cleaned_data of the nested formset and update the foreignkey reference
            for cd in nested.cleaned_data:
                cd[nested.fk.name] = instance
                
        return instance

    def should_delete(self, form):
        """Convenience method for determining if the form's object will
        be deleted; cribbed from BaseModelFormSet.save_existing_objects."""
 
        if self.can_delete:
            raw_delete_value = form._raw_value(DELETION_FIELD_NAME)
            should_delete = form.fields[DELETION_FIELD_NAME].clean(raw_delete_value)
            return should_delete
 
        return False
 
    def save_all(self, commit=True):
        """Save all formsets and along with their nested formsets."""
 
        # Save without committing (so self.saved_forms is populated)
        # -- We need self.saved_forms so we can go back and access
        #    the nested formsets
        objects = self.save(commit=False)
 
        # Save each instance if commit=True
        if commit:
            for o in objects:
                o.save()
 
        # save many to many fields if needed
        if not commit:
            self.save_m2m()
 
        # save the nested formsets
        for form in set(self.initial_forms + self.saved_forms):
            if self.should_delete(form): continue

            for nested in form.nested:
                nested.save(commit=commit)

GroupFormSet = inlineformset_factory(Menu, MenuGroup, form=MenuGroupForm, extra=20, can_delete=False)
MenuItemSizeFormSet = inlineformset_factory(MenuItem, MenuItemSize, form=MenuItemSizeForm, extra=10, can_delete=False)
DealItemFormSet = inlineformset_factory(Deal, DealItem, form=DealItemForm, extra=10, can_delete=False)

class BaseMenuFormSet(NestedInlineFormSet):

    nested_model_class = GroupFormSet
    prefix_class = 'GROUPS'

class BaseMenuItemFormSet(NestedInlineFormSet):

    nested_model_class = MenuItemSizeFormSet
    prefix_class = 'ITEMS'

class BaseDealFormSet(NestedInlineFormSet):

    nested_model_class = DealItemFormSet
    prefix_class = 'DEALS'

MenuFormSet = inlineformset_factory(Place, Menu, formset=BaseMenuFormSet, form=MenuForm, extra=5, can_delete=False)
MenuItemFormSet = inlineformset_factory(MenuGroup, MenuItem, formset=BaseMenuItemFormSet, form=MenuItemForm, extra=20, can_delete=False)
DealFormSet = inlineformset_factory(Place, Deal, formset=BaseDealFormSet, form=DealForm, extra=5, can_delete=False)
