# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'MenuItem.status'
        db.add_column('menus_menuitem', 'status',
                      self.gf('django.db.models.fields.CharField')(default='visible', max_length=20),
                      keep_default=False)

        # Adding field 'Menu.status'
        db.add_column('menus_menu', 'status',
                      self.gf('django.db.models.fields.CharField')(default='visible', max_length=20),
                      keep_default=False)

        # Adding field 'MenuItemSize.status'
        db.add_column('menus_menuitemsize', 'status',
                      self.gf('django.db.models.fields.CharField')(default='visible', max_length=20),
                      keep_default=False)

        # Adding field 'MenuGroup.status'
        db.add_column('menus_menugroup', 'status',
                      self.gf('django.db.models.fields.CharField')(default='visible', max_length=20),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'MenuItem.status'
        db.delete_column('menus_menuitem', 'status')

        # Deleting field 'Menu.status'
        db.delete_column('menus_menu', 'status')

        # Deleting field 'MenuItemSize.status'
        db.delete_column('menus_menuitemsize', 'status')

        # Deleting field 'MenuGroup.status'
        db.delete_column('menus_menugroup', 'status')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'menus.menu': {
            'Meta': {'object_name': 'Menu'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'duration_time_end': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'duration_time_start': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'menus'", 'to': "orm['places.Place']"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'visible'", 'max_length': '20'})
        },
        'menus.menugroup': {
            'Meta': {'object_name': 'MenuGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'groups'", 'to': "orm['menus.Menu']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'visible'", 'max_length': '20'})
        },
        'menus.menuitem': {
            'Meta': {'object_name': 'MenuItem'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '450', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'items'", 'to': "orm['menus.MenuGroup']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'visible'", 'max_length': '20'})
        },
        'menus.menuitemsize': {
            'Meta': {'object_name': 'MenuItemSize'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sizes'", 'to': "orm['menus.MenuItem']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'visible'", 'max_length': '20'})
        },
        'opinions.opinion': {
            'Meta': {'object_name': 'Opinion'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'opinions'", 'to': "orm['auth.User']"}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {}),
            'rating': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'submitted': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'places.city': {
            'Meta': {'object_name': 'City'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'places.place': {
            'Meta': {'object_name': 'Place'},
            'address_1': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True'}),
            'address_2': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True'}),
            'brief_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'places'", 'null': 'True', 'to': "orm['places.City']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '120', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'menus_cache': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'phone_2': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'restaurant_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'visible'", 'max_length': '20'}),
            'website_url': ('django.db.models.fields.URLField', [], {'max_length': '120', 'blank': 'True'})
        }
    }

    complete_apps = ['menus']