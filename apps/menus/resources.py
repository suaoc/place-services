from tastypie.authentication import Authentication
from tastypie.authorization import Authorization, ReadOnlyAuthorization
from tastypie.resources import ModelResource
from tastypie import fields, utils

from menus.models import Menu, MenuGroup, MenuItem, MenuItemSize, Deal, DealItem

class MenuResource(ModelResource):

    class Meta:
        authorization = Authorization()
        queryset = Menu.objects.all()
        resource_name = 'menus'

        always_return_data = True


class MenuSparseResource(ModelResource):

    groups = fields.OneToManyField('menus.resources.MenuGroupSparseResource', 'groups', null=True, full=True)

    class Meta:
        authentication = Authentication()
        authorization = ReadOnlyAuthorization()
        fields = ['name', 'description']
        include_resource_uri = False
        queryset = Menu.objects.all()
        resource_name = 'menus'


class MenuGroupSparseResource(ModelResource):

    items = fields.OneToManyField('menus.resources.MenuItemSparseResource', 'items', null=True, full=True)

    class Meta:
        authentication = Authentication()
        authorization = ReadOnlyAuthorization()
        fields = ['name', 'description']
        include_resource_uri = False
        queryset = MenuGroup.objects.all()
        resource_name = 'menu-groups'


class MenuItemSparseResource(ModelResource):

    sizes = fields.OneToManyField('menus.resources.MenuItemSizeSparseResource', 'sizes', null=True, full=True)

    class Meta:
        authentication = Authentication()
        authorization = ReadOnlyAuthorization()
        fields = ['name', 'description']
        include_resource_uri = False
        queryset = MenuItem.objects.all()
        resource_name = 'menu-items'


class MenuItemSizeSparseResource(ModelResource):

    class Meta:
        authentication = Authentication()
        authorization = ReadOnlyAuthorization()
        fields = ['name', 'price',]
        include_resource_uri = False
        queryset = MenuItemSize.objects.all()
        resource_name = 'menu-item-sizes'


class DealResource(ModelResource):

    items = fields.OneToManyField('menus.resources.DealItemResource', 'items', null=True, full=True)

    class Meta:
        authentication = Authentication()
        authorization = ReadOnlyAuthorization()
        fields = ['name', 'description', 'conditions', 'price',]
        include_resource_uri = False
        queryset = Deal.objects.all()
        resource_name = 'deals'

class DealItemResource(ModelResource):

    class Meta:
        authentication = Authentication()
        authorization = ReadOnlyAuthorization()
        fields = ['name',]
        include_resource_uri = False
        queryset = DealItem.objects.all()
        resource_name = 'deal-items'
