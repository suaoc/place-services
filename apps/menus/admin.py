from django.contrib import admin
from menus.models import Menu, MenuGroup, MenuItem, MenuItemSize


class MenuItemSizeInlineAdmin(admin.TabularInline):
    model = MenuItemSize

class MenuItemAdmin(admin.ModelAdmin):
    inlines = [
        MenuItemSizeInlineAdmin,
        ]

class MenuItemInlineAdmin(admin.TabularInline):
    model = MenuItem

class MenuGroupAdmin(admin.ModelAdmin):
    inlines = [
        MenuItemInlineAdmin,
        ]

class MenuGroupInlineAdmin(admin.TabularInline):
    model = MenuGroup

class MenuAdmin(admin.ModelAdmin):
    inlines = [
        MenuGroupInlineAdmin,
        ]

admin.site.register(MenuItem, MenuItemAdmin)
admin.site.register(MenuGroup, MenuGroupAdmin)
admin.site.register(Menu, MenuAdmin)

