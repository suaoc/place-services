# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Place'
        db.create_table('places_place', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('restaurant_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brief_description', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('address_1', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('address_2', self.gf('django.db.models.fields.CharField')(max_length=120, blank=True)),
            ('city_town', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
            ('phone_2', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
            ('website_url', self.gf('django.db.models.fields.URLField')(max_length=120, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=120, blank=True)),
        ))
        db.send_create_signal('places', ['Place'])


    def backwards(self, orm):
        # Deleting model 'Place'
        db.delete_table('places_place')


    models = {
        'places.place': {
            'Meta': {'object_name': 'Place'},
            'address_1': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'address_2': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True'}),
            'brief_description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'city_town': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '120', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'phone_2': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'restaurant_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'website_url': ('django.db.models.fields.URLField', [], {'max_length': '120', 'blank': 'True'})
        }
    }

    complete_apps = ['places']