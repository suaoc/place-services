from django.forms import ModelForm, widgets
from places.models import Place

class PlaceForm(ModelForm):

    class Meta:
        model = Place
        fields = ('status', 'restaurant_name', 'brief_description', 'address_1', 'city', 'latitude', 'longitude', 'phone', 'phone_2', 'website_url', 'email',)
        widgets = {
            'restaurant_name': widgets.TextInput(attrs={'class': 'place-name-input', 'placeholder': 'Name',}),
            'brief_description': widgets.Textarea(attrs={'class': 'place-brief-description-textarea', 'placeholder':'Tagline or Brief Description',}),
            'latitude': widgets.TextInput(attrs={'class': 'place-latitude-input', 'readonly': 'readonly',}),
            'longitude': widgets.TextInput(attrs={'class': 'place-longitude-input', 'readonly': 'readonly',}),
            }

