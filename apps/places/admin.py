from django.contrib import admin
from places.models import City, Place
from places.forms import PlaceForm
from opinions.admin import OpinionInline

class PlaceAdmin(admin.ModelAdmin):
    inlines = [
        OpinionInline,
        ]

admin.site.register(Place, PlaceAdmin)
admin.site.register(City)
