from django.db import models
from model_utils import Choices
from django.contrib.contenttypes import generic

from misc import search
from opinions.models import Opinion

STATUS = Choices(('visible', 'Visible'), ('hidden', 'Hidden'), ('deleted', 'Deleted'))

class City(models.Model):

    name = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = 'Cities'

    def __unicode__(self):
        return self.name


class PlaceManager(models.Manager):

    def live(self, search_query=None, city_name=None):

        places = self.all().filter(status__in=[STATUS.visible, STATUS.hidden])

        if search_query is not None and search_query != "":
            query_expression = search.get_query(search_query, ['restaurant_name',
                                                               'brief_description',
                                                               'address_1',
                                                               'menus_cache',
                                                               ])
            places = places.filter(query_expression)

        if city_name is not None and city_name != "":
            places = places.filter(city__name=city_name)

        return places


class Place(models.Model):

    status = models.CharField(choices=STATUS, default=STATUS.visible, max_length=20)

    restaurant_name = models.CharField(max_length=255, help_text='2-255 characters.')
    brief_description = models.CharField(max_length=255, blank=True, help_text='2-255 characters. Could be a tagline.')

    address_1 = models.CharField(max_length=120, blank=True, help_text='2-120 characters.')
    address_2 = models.CharField(max_length=120, blank=True, help_text='0-120 characters.')
    city = models.ForeignKey(City, null=True, related_name='places')

    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    phone = models.CharField(max_length=40, blank=True, help_text='format: 0XXX-XXXXXXX')
    phone_2 = models.CharField(max_length=40, blank=True, help_text='format: 0XXX-XXXXXXX')
    website_url = models.URLField(max_length=120, blank=True)
    email = models.EmailField(max_length=120, blank=True)

    menus_cache = models.TextField(blank=True)

    opinions = generic.GenericRelation(Opinion)

    objects = PlaceManager()

    def __unicode__(self):
        return self.restaurant_name + ' - ' + self.address_1 

    def average_rating(self):
        rating =  self.opinions.filter(rating__gte=1, rating__lte=5).aggregate(models.Avg('rating'))
        rating_avg = rating.get('rating__avg')
        if rating_avg is None:
            rating_avg = 0
        return round(rating_avg)
