from django.conf.urls import url
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.utils import simplejson
from tastypie.resources import ModelResource
from tastypie import fields, utils

from geopy import distance
from geopy.point import Point

from places.models import City, Place
from opinions.models import Opinion

class CityResource(ModelResource):

    class Meta:
        queryset = City.objects.all()
        resource_name = 'cities'


def bundle_opinions(bundle):
    try:
        opinions_limit =  int(bundle.request.GET.get('opinions_limit', '0'))
    except:
        opinions_limit = 0

    opinions_limit = min(opinions_limit, 20)

    return Opinion.objects.filter(author__is_active=True, place=bundle.obj).order_by('-id')[:opinions_limit]


class PlaceResource(ModelResource):

    city_town = fields.CharField(attribute='city', null=True)
    rating = fields.IntegerField(attribute='average_rating', readonly=True)
    opinions = fields.OneToManyField('opinions.resources.OpinionResource', attribute=lambda bundle: bundle_opinions(bundle), null=True, full=True)
    #menus    = fields.OneToManyField('menus.resources.MenuSparseResource', attribute='menus', null=True, full=True)
    deals    = fields.OneToManyField('menus.resources.DealResource', attribute='deals', null=True, full=True)

    class Meta:
        queryset = Place.objects.all()
        resource_name = 'places'
        excludes = ['menus_cache',]

    def get_object_list(self, request):
        search_query = request.GET.get('search_query', None)
        city_name = request.GET.get('city', None)
        return Place.objects.live(search_query=search_query, city_name=city_name)

    def get_list(self, request, **kwargs):
        objects = self.obj_get_list(request=request, **self.remove_api_resource_names(kwargs))
        sorted_objects = self.apply_sorting(objects, options=request.GET)

        try:
            user_latitude = float(request.GET.get('lat', ''))
            user_longitude = float(request.GET.get('lng', ''))
        except ValueError:
            pass
        else:
            user_location = Point(user_latitude, user_longitude)
            for place in sorted_objects:
                if place.latitude is not None and place.longitude is not None:
                    place_location = Point(place.latitude, place.longitude)
                    place.distance = distance.distance(user_location, place_location).km
                else:
                    place.distance = 99999

            sorted_objects = sorted(sorted_objects, key=lambda place: place.distance)

        paginator = self._meta.paginator_class(request.GET, sorted_objects, resource_uri=self.get_resource_uri(), limit=self._meta.limit, max_limit=self._meta.max_limit, collection_name=self._meta.collection_name)
        to_be_serialized = paginator.page()

        # Dehydrate the bundles in preparation for serialization.
        bundles = [self.build_bundle(obj=obj, request=request) for obj in to_be_serialized['objects']]
        to_be_serialized['objects'] = [self.full_dehydrate(bundle) for bundle in bundles]
        to_be_serialized = self.alter_list_data_to_serialize(request, to_be_serialized)
        return self.create_response(request, to_be_serialized)

    def dehydrate(self, bundle):

        # Include detail page link
        current_site = Site.objects.get_current()
        detail_page_path = reverse('interface_place_detail', kwargs={'place_id': bundle.obj.pk})
        bundle.data['link'] = 'http://%(domain)s%(path)s' % {'domain':current_site.domain, 'path':detail_page_path,}

        if len(bundle.data['opinions']) == 0:
            del bundle.data['opinions']

        if hasattr(bundle.obj, 'distance') and bundle.obj.distance < 1000:
            bundle.data['distance'] = bundle.obj.distance

        # Hack to cache menus
        if 'update_cache' in bundle.request.GET:
            from menus.resources import MenuSparseResource
            mr = MenuSparseResource()
            menu_bundles = [mr.build_bundle(obj=obj, request=bundle.request) for obj in bundle.obj.menus.all()]
            menus_to_be_serialized = [mr.full_dehydrate(menu_bundle) for menu_bundle in menu_bundles]
            menus_to_be_serialized = mr.alter_list_data_to_serialize(bundle.request, menus_to_be_serialized)
            
            desired_format = self.determine_format(bundle.request)
            menus_serialized = mr.serialize(bundle.request, menus_to_be_serialized, desired_format)
            bundle.obj.menus_cache = menus_serialized
            bundle.obj.save()
        
        try:
            menus_cache_json = simplejson.loads(bundle.obj.menus_cache)
        except:
            pass
        else:
            bundle.data['menus'] = menus_cache_json

        return bundle


    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/(?P<pk>\w[\w/-]*)/opinions%s$" % (self._meta.resource_name, utils.trailing_slash()), self.wrap_view('get_opinions'), name="api_get_opinions"),
            ]

    def get_opinions(self, request, **kwargs):
        from opinions.resources import OpinionResource

        try:
            obj = self.cached_obj_get(request=request, **self.remove_api_resource_names(kwargs))
        except ObjectDoesNotExist:
            return HttpGone()
        except MultipleObjectsReturned:
            return HttpMultipleChoices("More than one resource is found at this URI.")

        opinion_resource = OpinionResource()
        obj_content_type = ContentType.objects.get_for_model(obj)
        return opinion_resource.get_list(request, parent_content_type=obj_content_type, parent_object_id=obj.id)
