from django.shortcuts import get_object_or_404, redirect, render_to_response
from django.template import RequestContext

from places.models import Place

def HomeView(request):
    return render_to_response("home.html", {
                              }, context_instance=RequestContext(request))


def PlaceDetailView(request, place_id=None):

    place = get_object_or_404(Place, id=place_id)

    return render_to_response("place_detail.html", {
                              'place': place,
                              }, context_instance=RequestContext(request))
