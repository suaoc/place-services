from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('',
                       url(r'^$', 'interface.views.HomeView', name='interface_home'),
                       url(r'^places/(?P<place_id>[\d]+)/$', 'interface.views.PlaceDetailView', name='interface_place_detail'),
                       )

